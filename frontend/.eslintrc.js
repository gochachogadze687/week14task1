module.exports = {
    extends: 'airbnb-base',
    rules: {
        indent: ['error', 4],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        'no-console': 'off',
        'no-unused-vars': 'warn',
        'arrow-parens': ['error', 'as-needed'],
        'no-restricted-syntax': ['error', 'ForInStatement', 'LabeledStatement', 'WithStatement'],
        'no-alert': 'error',
        'max-len': ['error', { code: 120 }],
        'no-plusplus': 'off',
    },
};
