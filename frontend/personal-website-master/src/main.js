import './email-validator.js';   
import './website-section.js';   
import { setupEmailInput } from './join-us-section.js';
import './styles/style.css';





const joinForm = document.getElementById('joinForm');
const unsubscribeContainer = document.getElementById('unsubscribeContainer');
const unsubscribeButton = document.getElementById('unsubscribeButton');
const worker = new Worker(new URL('./worker.js', import.meta.url));



async function handleUnsubscribe() {
  try {
    unsubscribeButton.disabled = true;
    unsubscribeButton.style.opacity = '0.5';
    const response = await fetch('/unsubscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: emailInput.value }),
    });

    if (response.ok) {
      emailInput.style.display = 'block';
      unsubscribeContainer.innerHTML = '';
      unsubscribeButton.style.display = 'none'; 
      joinForm.style.display = 'block'; 
      localStorage.removeItem('subscriptionEmail');
    } else {
      const errorMessage = document.createElement('p');
      errorMessage.textContent = 'Failed to unsubscribe: ' + response.statusText;
      errorMessage.style.color = 'red';
      unsubscribeContainer.appendChild(errorMessage);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  } finally {
    unsubscribeButton.disabled = false;
    unsubscribeButton.style.opacity = '1'; 
  }
}


async function handleFormSubmit(e) {
  e.preventDefault();

  try {
    const response = await fetch('/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: emailInput.value }),
    });

    if (response.ok) {
      hideEmailInputAndShowButton();
      joinForm.style.display = 'none';
    } else if (response.status === 422) {
      const errorData = await response.json();
      const errorContainer = document.getElementById('errorContainer');
      errorContainer.textContent = errorData.error;
      errorContainer.style.display = 'block'; 
    } else {
      console.error('Failed to subscribe:', response.statusText);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}




document.addEventListener('DOMContentLoaded', () => {
  const emailInput = document.getElementById('email'); 

  setupEmailInput();

  const subscribeButton = document.getElementById('subscribe-button');

  if (subscribeButton) {
    subscribeButton.addEventListener('click', () => {
      console.log('Subscribe button clicked');
      worker.postMessage({ type: 'subscribeButtonClicked' });
    });
  }

  if (emailInput) {
    emailInput.addEventListener('input', () => {
    });
  }
});